package com.dosideas.camel.demo.flow;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * Varios ejemplos de diferentes componentes de Camel.
 *
 * Los RouteBuilder se agregan automaticamente al contexto de Apache Camel, y
 * quedan disponibles una vez iniciado el contexto de Spring.
 *
 * Leer más : http://camel.apache.org/spring-boot.html
 *
 */
@Component
public class DemoRoute extends RouteBuilder {

    /**
     * Configuración de los routes. Idealmente existe un route por cada RouteBuilder.
     * En esta clase de ejemplo se configuran diferentes routes de demo, con
     * distintos componentes y características. Se pueden comentar los routes que
     * se quieran ignorar, para facilitar la comprensión de la demo.
     */
    @Override
    public void configure() throws Exception {
        holaMundoPorConsolaDirecto();
//        holaMundoPorConsolaConBean();
//        holaMundoPorLogConTimer();
//
//        querySimpleConJdbc();
//        querySimpleConSql();
//        querySimpleConSqlYParametro();
//        queryConSplit();
//        queryConSplitJoin();
//
//        manejoDeErroresConTryCatchFinally();
//        splitConErrores();
    }

    /**
     * Imprime un mensaje cuando llega un mensaje directo.
     * La expresión ${body} hace referencia al cuerpo del mensaje recibido.
     * El mensaje de ejemplo se lanza después de que inicia Spring Boot,
     * en la clase ApplicationStartup.
     *
     * Leer más: http://camel.apache.org/direct.html
     */
    private void holaMundoPorConsolaDirecto() {
        from("direct:holaMundo")
                .log("${body}");
    }

    /**
     * Envia el body del mensaje al método de un bean de Spring, y continua el
     * procesamiento con el resultado de la invocacion a este método.
     * El mensaje de ejemplo se lanza después de que inicia Spring Boot,
     * en la clase ApplicationStartup.
     *
     * Leer más: http://camel.apache.org/bean.html
     */
    private void holaMundoPorConsolaConBean() {
        from("direct:holaMundoMayusculas")
                .to("bean:personajeService?method=aMayusculas")
                .log("${body}");
    }

    /**
     * Imprime por consola (log:out) un string simple. Un timer es quien lanza
     * automáticamente esta actividad.
     * El destino "log" se encarga de pasar el mensaje al framework de logging
     * sfl4j, y loguea el mensaje completo.
     *
     * Leer más: http://camel.apache.org/timer.html
     * Leer más: http://camel.apache.org/log.html
     */
    private void holaMundoPorLogConTimer() {
        from("timer://holaMundoTimer?period=2000")
                .setBody()
                .simple("Hola, mundo! ${header.firedTime}")
                .to("log:out");
    }

    /**
     * Ejecuta un query SQL a una base de datos. El datasource es un bean configurado
     * por Spring con el nombre "dataSource" (es el datasource por default, que
     * se crea automáticamente). El mensaje que contiene todas las filas del
     * resultado se envia a imprimir.
     *
     * Leer más: http://camel.apache.org/jdbc.html
     */
    private void querySimpleConJdbc() {
        from("timer://personajesJdbcTimer?period=5000")
                .setBody(constant("select * from personaje"))
                .to("jdbc:dataSource")
                .to("log:out");
    }

    /**
     * Ejecuta un query SQL a una base de datos. El datasource es un bean configurado
     * por Spring con el nombre "dataSource" (es el datasource por default, que
     * se crea automáticamente). El mensaje que contiene todas las filas del
     * resultado se envia a imprimir.
     *
     * Leer más: http://camel.apache.org/sql-component.html
     */
    private void querySimpleConSql() {
        from("timer://personajesJdbcTimer?period=5000")
                .to("sql:select * from personaje?dataSource=dataSource")
                .to("log:out");
    }

    /**
     * Ejecuta un query SQL a una base de datos usando parametros del body
     * del mensaje.
     */
    private void querySimpleConSqlYParametro() {
        from("timer://personajesJdbcTimer?period=5000")
                .setBody(constant("Zim"))
                .to("sql:select * from personaje where nombre = :#${body} ?dataSource=dataSource")
                .to("log:out");
    }

    /**
     * Ejecuta un query SQL a una base de datos, y realiza un split del resultado,
     * de manera que las filas son procesadas independientemente.
     * El método end() finaliza todas las rutas del split(). Como no hay más
     * rutas, no es necesario agregarlo.
     */
    private void queryConSplit() {
        from("timer://personajesJdbcTimer?period=5000")
                .setBody(constant("select * from personaje"))
                .to("jdbc:dataSource")
                .split(body())
                    .log("Fila: ${body[ID]} - ${body[NOMBRE]} - ${body[SERIE]}")
                    .to("log:out")
                .end();
    }

    /**
     * Ejecuta un query mediante un timer, realiza un split para procesar cada
     * registro, y luego de procesar todos hace un "join" del split y continua.
     * El método end() finaliza todas las rutas de los mensajes del split(),
     * y crea un nuevo mensaje con todos los mensajes resultantes del split().
     * Es decir, es un "join" del split.
     *
     * Leer más: http://camel.apache.org/composed-message-processor.html
     */
    private void queryConSplitJoin() {
        from("timer://personajesJdbcTimer?period=5000")
                .setBody(constant("select * from personaje"))
                .to("jdbc:dataSource")
                .split(body())
                    .log("Fila: ${body[ID]} - ${body[NOMBRE]} - ${body[SERIE]}")
                .end()
                .to("log:out");
    }

    /**
     * Crea un bloque try-catch-finally para atender el mensaje.
     *
     * Leer más: http://camel.apache.org/try-catch-finally.html
     */
    private void manejoDeErroresConTryCatchFinally() {
        from("direct:errorConReintentos")
                .doTry()
                    .log("Invocando bean que falla...")
                    .to("bean:personajeService?method=metodoQueFalla")
                .doCatch(UnsupportedOperationException.class)
                    .log("Ocurrio un error...")
                .doFinally()
                    .log("Bloque finally");
    }

    /**
     * Un split donde si ocurre un fallo se captura para procesar. Todos los
     * elementos del split se procesan, pero si se lanza una excepcion no ocurre
     * el join, y se captura la excepcion.
     */
    private void splitConErrores() {
        from("direct:splitConErrores")
                .setBody(constant("select * from personaje"))
                .to("jdbc:dataSource")
                .doTry()
                    .split(body())
                        .to("bean:personajeService?method=soloPares(${body[ID]})")
                        .log("procese bien el ${body[ID]}")
                    .end()
                    .log("Se proceso todo bien, todos fueron numeros pares!")
                .endDoTry()
                .doCatch(IllegalArgumentException.class)
                    .log("No se procesaron todos bien: llegó un numero impar");
    }

}
