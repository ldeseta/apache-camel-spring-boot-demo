package com.dosideas.camel.demo;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.MockEndpoints;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest
@MockEndpoints
public class RouteParaTestTest {

    @Autowired
    private ModelCamelContext context;

    @Produce(uri = "direct:holaMundoTest")
    private ProducerTemplate producerTemplate;
    
    @Test
    public void contextLoads() throws InterruptedException {
        String body = "hola, mundo!";
        MockEndpoint resultEndpoint = context.getEndpoint("mock:log:out", MockEndpoint.class);
        resultEndpoint.expectedBodiesReceived(body);

        producerTemplate.sendBody(body);

        resultEndpoint.assertIsSatisfied();
    }

}
