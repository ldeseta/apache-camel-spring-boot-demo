package com.dosideas.camel.demo.flow;

import com.dosideas.camel.domain.Persona;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class RestFlowCamel extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        restConfiguration().port(8080).bindingMode(RestBindingMode.json);

        rest("/hello")
                .post()
                .consumes("application/json")
                .produces("text/plain")
                .type(Persona.class)
                .to("direct:saludar");

        from("direct:saludar")
                .log("${body}")
                .transform().simple("hola, mundo para el ${body.nombre}");
    }
}
