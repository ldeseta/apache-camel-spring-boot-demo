package com.dosideas.camel.demo;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    /**
     * El contexto general de Camel. Contiene utilidades generales para todos
     * los endpoints. 
     */
    private final CamelContext camelContext;
    /**
     * La autoconfiguracion de Camel brinda un ProducerTemplate y un
     * ConsumerTemplate listos para usar. El ProducerTemplate se puede utilizar
     * para enviar un mensaje a algún endpoint.
     */
    private final ProducerTemplate producerTemplate;

    public ApplicationStartup(CamelContext camelContext, ProducerTemplate producerTemplate) {
        this.camelContext = camelContext;
        this.producerTemplate = producerTemplate;
    }

    /**
     * Este evento se ejecuta cuando la aplicación de Spring está lista para
     * recibir peticiones.
     *
     * ProducerTemplate.sendBody() es bloqueante y espera el envio del mensaje.
     * Si no hay nadie atendiendo, se queda bloqueado. Para evitar esto, el 
     * método chequea primero si el endpoint existe (de esta manera se pueden
     * comentar los route defindos en DemoRoute y no hay bloqueos en esta clase).
     */
    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        if (camelContext.hasEndpoint("direct:holaMundo") != null) {
            producerTemplate.sendBody("direct:holaMundo", "Hola, mundo! Desde ApplicationStartup.");
        }
        if (camelContext.hasEndpoint("direct:holaMundoMayusculas") != null) {
            producerTemplate.sendBody("direct:holaMundoMayusculas", "Hola, mundo! Desde ApplicationStartup, va a pasarse a mayusculas.");
        }
        if (camelContext.hasEndpoint("direct:errorConReintentos") != null) {
            producerTemplate.sendBody("direct:errorConReintentos", "Este mensaje va a fallar.");
        }
        if (camelContext.hasEndpoint("direct:splitConErrores") != null) {
            producerTemplate.sendBody("direct:splitConErrores", null);
        }
    }

}
