drop table personaje if exists;

create table personaje (
    id integer identity,
    nombre varchar not null,
    serie varchar not null
);

insert into personaje 
(nombre,    serie) values
('Zim',     'Invasor Zim'),
('Dib',     'Invasor Zim'),
('Gaz',     'Invasor Zim'),
('Dexter',  'El laboratorio de Dexter'),
('Didi',    'El laboratorio de Dexter');